package com.thoughtworks.collection;

import java.util.List;
import java.util.Objects;

public class StreamReduce {

    public int getLastOdd(List<Integer> numbers) {
        return numbers.stream().reduce(0, (prev, number)->{
            if (number%2!=0) prev = number;
            return prev;
        });
    }

    public String getLongest(List<String> words) {
        return words.stream().reduce(words.get(0), (prev, word)->{
            if(word.length()>prev.length()) prev = word;
            return prev;
        });
    }

    public int getTotalLength(List<String> words) {
//        return words.stream().map(word->word.length()).reduce(0, Integer::sum);
        return words.stream().reduce(0, (prev, word)->prev+word.length(), Integer::sum);
    }
}
